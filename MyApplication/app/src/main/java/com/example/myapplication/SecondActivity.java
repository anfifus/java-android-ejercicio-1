package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    private EditText username = findViewById(R.id.editTextUserName);
    private EditText pass = findViewById(R.id.editTextPass);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_second);
        Intent intent = getIntent();//Consigue el intent cuando se habia hecho el cambio de actividad
        Bundle bound = intent.getExtras();//Consigue los parametros que habiamos insertado en la actividad anteniror
        username.setText(bound.getString("username"));//Añade el nombre de usuario obteniendo mediante la clave asignada anteriormente
        pass.setText(bound.getString("pass"));//Añade la clave del usuario obteniendo mediante la clave asignada anteriormente
    }

}
