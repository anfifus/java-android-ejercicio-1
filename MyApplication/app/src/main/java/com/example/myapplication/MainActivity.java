package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity{

    //Variables que son recogidas del layout mediante el id
    private Button boton = findViewById(R.id.buttonEnviar);
    private EditText userName = findViewById(R.id.editTextUserName);
    private EditText pass = findViewById(R.id.editTextPass) ;
    //-----------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainform);//Indica la actividad inicial
        boton.setOnClickListener(new View.OnClickListener(){//Crea una clase anonima OnClickListener para el objeto boton

            @Override
            public void onClick(View view){//Metodo que se ejecutara cuando se inicie el evento onClick del objeto boton
                String name = userName.getText().toString();
                String password = pass.getText().toString();
                Intent intent = new Intent(view.getContext(),SecondActivity.class);//Permite transitar entre la actividad actual a otra de nuestra eleccion
                Bundle bound = new Bundle();//Creamos un objeto Bundle para contener varias variables a pasar
                bound.putString("username",name);//Contiene la palabra clave con que haremos referencia y el contenido que necesitamos pasar
                bound.putString("pass",password);//Contiene la palabra clave con que haremos referencia y el contenido que necesitamos pasar
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    startActivity(intent,bound);//Inicia la actividad
                }
            }
        });
    }
}
